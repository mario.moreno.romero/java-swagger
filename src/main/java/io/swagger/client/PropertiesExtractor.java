package io.swagger.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class PropertiesExtractor {
    private static Properties properties;
    static {
        properties = new Properties();
        URL url = PropertiesExtractor.class.getClassLoader().getResource("application.properties");
        try{
            properties.load(new FileInputStream(url.getPath()));
        } catch (IOException e) {
            properties = null;
        }
    }
    PropertiesExtractor(){
        //comentario para evitar sonar
    }
    public static String getProperty(String key){
        return properties.getProperty(key);
    }
}