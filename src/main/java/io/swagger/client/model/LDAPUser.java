/*
 * WebApiSegura Token JWT XrmAPI
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * LDAPUser
 */

public class LDAPUser {
  @SerializedName("type")
  private String type = null;

  @SerializedName("ou")
  private String ou = null;

  @SerializedName("cn")
  private List<String> cn = null;

  @SerializedName("givenname")
  private String givenname = null;

  @SerializedName("sn")
  private String sn = null;

  @SerializedName("telephonenumber")
  private String telephonenumber = null;

  @SerializedName("mail")
  private String mail = null;

  @SerializedName("userpassword")
  private String userpassword = null;

  public LDAPUser type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public LDAPUser ou(String ou) {
    this.ou = ou;
    return this;
  }

   /**
   * Get ou
   * @return ou
  **/
  @ApiModelProperty(value = "")
  public String getOu() {
    return ou;
  }

  public void setOu(String ou) {
    this.ou = ou;
  }

  public LDAPUser cn(List<String> cn) {
    this.cn = cn;
    return this;
  }

  public LDAPUser addCnItem(String cnItem) {
    if (this.cn == null) {
      this.cn = new ArrayList<>();
    }
    this.cn.add(cnItem);
    return this;
  }

   /**
   * Get cn
   * @return cn
  **/
  @ApiModelProperty(value = "")
  public List<String> getCn() {
    return cn;
  }

  public void setCn(List<String> cn) {
    this.cn = cn;
  }

  public LDAPUser givenname(String givenname) {
    this.givenname = givenname;
    return this;
  }

   /**
   * Get givenname
   * @return givenname
  **/
  @ApiModelProperty(value = "")
  public String getGivenname() {
    return givenname;
  }

  public void setGivenname(String givenname) {
    this.givenname = givenname;
  }

  public LDAPUser sn(String sn) {
    this.sn = sn;
    return this;
  }

   /**
   * Get sn
   * @return sn
  **/
  @ApiModelProperty(value = "")
  public String getSn() {
    return sn;
  }

  public void setSn(String sn) {
    this.sn = sn;
  }

  public LDAPUser telephonenumber(String telephonenumber) {
    this.telephonenumber = telephonenumber;
    return this;
  }

   /**
   * Get telephonenumber
   * @return telephonenumber
  **/
  @ApiModelProperty(value = "")
  public String getTelephonenumber() {
    return telephonenumber;
  }

  public void setTelephonenumber(String telephonenumber) {
    this.telephonenumber = telephonenumber;
  }

  public LDAPUser mail(String mail) {
    this.mail = mail;
    return this;
  }

   /**
   * Get mail
   * @return mail
  **/
  @ApiModelProperty(value = "")
  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public LDAPUser userpassword(String userpassword) {
    this.userpassword = userpassword;
    return this;
  }

   /**
   * Get userpassword
   * @return userpassword
  **/
  @ApiModelProperty(value = "")
  public String getUserpassword() {
    return userpassword;
  }

  public void setUserpassword(String userpassword) {
    this.userpassword = userpassword;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LDAPUser ldAPUser = (LDAPUser) o;
    return Objects.equals(this.type, ldAPUser.type) &&
        Objects.equals(this.ou, ldAPUser.ou) &&
        Objects.equals(this.cn, ldAPUser.cn) &&
        Objects.equals(this.givenname, ldAPUser.givenname) &&
        Objects.equals(this.sn, ldAPUser.sn) &&
        Objects.equals(this.telephonenumber, ldAPUser.telephonenumber) &&
        Objects.equals(this.mail, ldAPUser.mail) &&
        Objects.equals(this.userpassword, ldAPUser.userpassword);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, ou, cn, givenname, sn, telephonenumber, mail, userpassword);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LDAPUser {\n");
    
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    ou: ").append(toIndentedString(ou)).append("\n");
    sb.append("    cn: ").append(toIndentedString(cn)).append("\n");
    sb.append("    givenname: ").append(toIndentedString(givenname)).append("\n");
    sb.append("    sn: ").append(toIndentedString(sn)).append("\n");
    sb.append("    telephonenumber: ").append(toIndentedString(telephonenumber)).append("\n");
    sb.append("    mail: ").append(toIndentedString(mail)).append("\n");
    sb.append("    userpassword: ").append(toIndentedString(userpassword)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

