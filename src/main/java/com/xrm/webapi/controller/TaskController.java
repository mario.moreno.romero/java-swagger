package com.xrm.webapi.controller;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import io.swagger.client.api.TaskApi;
import io.swagger.client.ApiException; 
import javax.servlet.http.HttpServletResponse;
import io.swagger.client.model.CTaskAttributes; 

@CrossOrigin
@RestController
@EnableAutoConfiguration
@RequestMapping("/api/task")
public class TaskController {
	static final String RESULT_ERROR_START="{'result':false,'message':'";
	static final String RESULT_ERROR_END="'}";
	
    @GetMapping(value = "get", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getAll(
            @RequestParam Integer index,
            @RequestParam Integer limit,
            @RequestParam(required = false) String q,
            @RequestParam(required = false) String orderby,
            @RequestParam(required = false) String order,
            @RequestParam(required = false) String fields,
            @RequestParam(required = false) String filters,
            @RequestParam(required = false) Boolean fetchcount,
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
        try {
            TaskApi apiInstance = new TaskApi();
            response = apiInstance.taskGet(index, limit, q, orderby, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }


    @GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getById(@PathVariable("id") String id, @RequestHeader("Authorization") String authorization, HttpServletResponse http) {
        Object response= new Object();
        try {
            TaskApi apiInstance = new TaskApi();
            response = apiInstance.taskGet0(id, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object deleteById(
            @PathVariable("id") String id,
            @RequestParam String statecode,
            @RequestParam String statuscode,
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
        try {
            TaskApi apiInstance = new TaskApi();
            apiInstance.taskDelete(id, statecode, statuscode, authorization);

            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @PutMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object updateById(
            @PathVariable("id") String id,
            @RequestHeader("Authorization") String authorization,
            @RequestBody final CTaskAttributes value, HttpServletResponse http) {
        Object response= new Object();
        try {
            TaskApi apiInstance = new TaskApi();
            response = apiInstance.taskPut(id, value, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Object save(
            @RequestHeader("Authorization") String authorization,
            @RequestBody final CTaskAttributes value, HttpServletResponse http) {
        Object response= new Object();
        try {
            TaskApi apiInstance = new TaskApi();
            response = apiInstance.taskPost(value, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @GetMapping(value = "getcatalogs", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getCatalogs( 
            @RequestParam String catalogs, 
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
        try {
            TaskApi apiInstance = new TaskApi();
            response = apiInstance.taskGetCatalogs(catalogs, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }
}
