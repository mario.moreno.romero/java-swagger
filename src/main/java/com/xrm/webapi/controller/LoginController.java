package com.xrm.webapi.controller;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import io.swagger.client.api.LoginApi;
import io.swagger.client.ApiException;
import io.swagger.client.model.CFirmante;
import io.swagger.client.model.LoginRequest;
import io.swagger.client.model.LDAPLoginRequest;
import io.swagger.client.model.LDAPUser;
import javax.servlet.http.HttpServletResponse;
import io.swagger.client.model.CSeguimientoContratacionPortalAttributes;

@CrossOrigin
@RestController
@EnableAutoConfiguration
@RequestMapping("/api/login")
public class LoginController {
	static final String RESULT_ERROR_START="{'result':false,'message':'";
	static final String RESULT_ERROR_END="'}";
	

    @GetMapping(value = "firmante",  produces = MediaType.APPLICATION_JSON_VALUE)
    public Object firmante(
            @RequestBody final CFirmante firmante, 
            HttpServletResponse http) {
        Object response= new Object();
        try {
            LoginApi apiInstance = new LoginApi();
            response = apiInstance.loginFirmantes(firmante, "");
            
            
        } catch (ApiException e) {
            response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @PostMapping( produces = MediaType.APPLICATION_JSON_VALUE, value = "doc2signfirmantes")
    public Object doc2signFirmantes(  HttpServletResponse http) {
        Object response= new Object();
        try {
            LoginApi apiInstance = new LoginApi();
            response = apiInstance.loginDoc2signFirmantes("");
            
            
        } catch (ApiException e) {

            response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @PostMapping( produces = MediaType.APPLICATION_JSON_VALUE, value = "doc2signdocstipos")
    public Object doc2signDocsTipos(  HttpServletResponse http) {
        Object response= new Object();
        try {
            LoginApi apiInstance = new LoginApi();
            response = apiInstance.loginDoc2signDocsTipos("");
            
            
        } catch (ApiException e) {

            response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @PostMapping( produces = MediaType.APPLICATION_JSON_VALUE, value = "addcreateuser")
    public Object addCreateUser(
            @RequestBody final LDAPUser ldapUser,  HttpServletResponse http) {
        Object response= new Object();
        try {
            LoginApi apiInstance = new LoginApi();
            response = apiInstance.loginAddCreatUser(ldapUser, "");
            
            
        } catch (ApiException e) {

            response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @PostMapping( produces = MediaType.APPLICATION_JSON_VALUE, value = "adresetpwd")
    public Object adresetPwd(
            @RequestBody final LDAPLoginRequest ldapUser,  HttpServletResponse http) {
        Object response= new Object();
        try {
            LoginApi apiInstance = new LoginApi();
            response = apiInstance.loginAdresetPwd(ldapUser, "");
            
            
        } catch (ApiException e) {

            response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @PostMapping( produces = MediaType.APPLICATION_JSON_VALUE, value = "adchangepwd")
    public Object adchangePwd(
            @RequestBody final LDAPLoginRequest ldapUser,
            @RequestHeader("Authorization") String authorization, HttpServletResponse http) {
        Object response= new Object();
        try {
            LoginApi apiInstance = new LoginApi();
            response = apiInstance.loginADChangePwd(ldapUser, authorization);
            
            
        } catch (ApiException e) {

            response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @PostMapping( produces = MediaType.APPLICATION_JSON_VALUE, value = "adlogin")
    public Object adLogin(
            @RequestBody final LDAPLoginRequest ldapUser,  HttpServletResponse http) {
        Object response= new Object();
        try {
            LoginApi apiInstance = new LoginApi();
            response = apiInstance.loginADLogin(ldapUser, "");
            
            
        } catch (ApiException e) {

            response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @PostMapping( produces = MediaType.APPLICATION_JSON_VALUE, value = "authenticate")
    public Object authenticate(
            @RequestBody final LoginRequest login,  HttpServletResponse http) {
		Object response= new Object();
        try {
            LoginApi apiInstance = new LoginApi();
			response =  apiInstance.loginAuthenticate(login, "");

        } catch (ApiException e) {
			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
            
        }

        return response;
    }

    @PutMapping(value = "/{id}",  produces = MediaType.APPLICATION_JSON_VALUE)
    public Object updateById(
            @PathVariable("id") String id, 
            @RequestBody final CSeguimientoContratacionPortalAttributes value, HttpServletResponse http) {
        Object response= new Object();
        try {
            LoginApi apiInstance = new LoginApi();
            response = apiInstance.loginChangePwd(id, value, "");
            
            
        } catch (ApiException e) {
            response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

}
