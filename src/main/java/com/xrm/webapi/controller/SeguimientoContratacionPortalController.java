package com.xrm.webapi.controller;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import io.swagger.client.api.SeguimientoContratacionPortalApi;
import io.swagger.client.ApiException;
import io.swagger.client.model.AttachmentData;
import io.swagger.client.WebserviceParameters;
import javax.servlet.http.HttpServletResponse;
import io.swagger.client.model.CSeguimientoContratacionPortalAttributes;
import java.util.List;

@CrossOrigin
@RestController
@EnableAutoConfiguration
@RequestMapping("/api/seguimientocontratacionportal")
public class SeguimientoContratacionPortalController {
	static final String RESULT_ERROR_START="{'result':false,'message':'";
	static final String RESULT_ERROR_END="'}";
	
    @GetMapping(value = "get",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getAll(
            @RequestParam Integer index,
            @RequestParam Integer limit,
            @RequestParam(required = false) String q,
            @RequestParam(required = false) String orderby,
            @RequestParam(required = false) String order,
            @RequestParam(required = false) String fields,
            @RequestParam(required = false) String filters,
            @RequestParam(required = false) Boolean fetchcount,
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
        try {
            SeguimientoContratacionPortalApi apiInstance = new SeguimientoContratacionPortalApi();
			WebserviceParameters webserviceParameters= new WebserviceParameters();
			webserviceParameters.setIndex(index);
			webserviceParameters.setLimit(limit);
			webserviceParameters.setQ(q);
			webserviceParameters.setOrderby(orderby);
			webserviceParameters.setOrder(order);
			webserviceParameters.setFields(fields);
			webserviceParameters.setFilters(filters);
			webserviceParameters.setFetchcount(fetchcount);
            response = apiInstance.seguimientoContratacionPortalGet(webserviceParameters, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "clearcache")
    public Object clearcache(  HttpServletResponse http) {
        Object response= new Object();
        try {
            SeguimientoContratacionPortalApi apiInstance = new SeguimientoContratacionPortalApi();
            response = apiInstance.seguimientoContratacionPortalClearcache("");

            
            
        } catch (ApiException e) {
			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getById(@PathVariable("id") String id, @RequestHeader("Authorization") String authorization, HttpServletResponse http) {
        
		Object response= new Object();
        try {
            SeguimientoContratacionPortalApi apiInstance = new SeguimientoContratacionPortalApi();            
			response= apiInstance.seguimientoContratacionPortalGet0(id, authorization);
            
        } catch (ApiException e) {
            response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
            			
        }
        return response;
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object deleteById(
            @PathVariable("id") String id,
            @RequestParam String statecode,
            @RequestParam String statuscode,
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
        try {
            SeguimientoContratacionPortalApi apiInstance = new SeguimientoContratacionPortalApi();
            apiInstance.seguimientoContratacionPortalDelete(id, statecode, statuscode, authorization);

            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object updateById(
            @PathVariable("id") String id,
            @RequestHeader("Authorization") String authorization,
            @RequestBody final CSeguimientoContratacionPortalAttributes seguimiento, HttpServletResponse http) {
        Object response= new Object();
        try {
            SeguimientoContratacionPortalApi apiInstance = new SeguimientoContratacionPortalApi();
            response = apiInstance.seguimientoContratacionPortalPut(id, seguimiento, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Object save(
            @RequestHeader("Authorization") String authorization,
            @RequestBody final CSeguimientoContratacionPortalAttributes seguimiento, HttpServletResponse http) {
        Object response= new Object();
        try {
            SeguimientoContratacionPortalApi apiInstance = new SeguimientoContratacionPortalApi();
            response = apiInstance.seguimientoContratacionPortalPost(seguimiento, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @PostMapping(value = "deletefile", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object deleteFile(
            @RequestParam String filename,
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
        try {
            SeguimientoContratacionPortalApi apiInstance = new SeguimientoContratacionPortalApi();
            response = apiInstance.seguimientoContratacionPortalDeleteFile(filename, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @PostMapping(value = "uploadfiles", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object uploadFiles(
            @RequestBody final List<AttachmentData> files,
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
        try {
            SeguimientoContratacionPortalApi apiInstance = new SeguimientoContratacionPortalApi();
            response = apiInstance.seguimientoContratacionPortalUploadFiles(files, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

}
