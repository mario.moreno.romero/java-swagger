package com.xrm.webapi.controller;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import io.swagger.client.api.SolicitarrcApi;
import io.swagger.client.ApiException;
import javax.servlet.http.HttpServletResponse;
import io.swagger.client.model.CSolicitarRCAttributes;
import io.swagger.client.WebserviceParameters;


@CrossOrigin
@RestController
@EnableAutoConfiguration
@RequestMapping("/api/solicitarrc")
public class SolicitarRCController {
	static final String RESULT_ERROR_START="{'result':false,'message':'";
	static final String RESULT_ERROR_END="'}";
	
    @GetMapping(value = "get", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getAll(
            @RequestParam Integer index,
            @RequestParam Integer limit,
            @RequestParam(required = false) String q,
            @RequestParam(required = false) String orderby,
            @RequestParam(required = false) String order,
            @RequestParam(required = false) String fields,
            @RequestParam(required = false) String filters,
            @RequestParam(required = false) Boolean fetchcount,
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
		
	
        try {
            SolicitarrcApi apiInstance = new SolicitarrcApi();
			WebserviceParameters webserviceParameters= new WebserviceParameters();
			webserviceParameters.setIndex(index);
			webserviceParameters.setLimit(limit);
			webserviceParameters.setQ(q);
			webserviceParameters.setOrderby(orderby);
			webserviceParameters.setOrder(order);
			webserviceParameters.setFields(fields);
			webserviceParameters.setFilters(filters);
			webserviceParameters.setFetchcount(fetchcount);
            response = apiInstance.solicitarrcGet(webserviceParameters, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }


@GetMapping(value = "getcodigopostallookup",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getCodigopostallookup(
            @RequestParam Integer index,
            @RequestParam Integer limit,
            @RequestParam(required = false) String q,
            @RequestParam(required = false) String orderby,
            @RequestParam(required = false) String order,
            @RequestParam(required = false) String fields,
            @RequestParam(required = false) String filters,
            @RequestParam(required = false) Boolean fetchcount,
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
		
	
        try {
            SolicitarrcApi apiInstance = new SolicitarrcApi();
			WebserviceParameters webserviceParameters1= new WebserviceParameters();
			webserviceParameters1.setIndex(index);
			webserviceParameters1.setLimit(limit);
			webserviceParameters1.setQ(q);
			webserviceParameters1.setOrderby(orderby);
			webserviceParameters1.setOrder(order);
			webserviceParameters1.setFields(fields);
			webserviceParameters1.setFilters(filters);
			webserviceParameters1.setFetchcount(fetchcount);
            response = apiInstance.solicitarrcGetCodigoPostalLookup(webserviceParameters1, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }
	
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "clearcache")
    public Object clearcache(  HttpServletResponse http) {
        Object response= new Object();
        try {
            SolicitarrcApi apiInstance = new SolicitarrcApi();
            response = apiInstance.solicitarrcClearcache("");

            
            
        } catch (ApiException e) {

            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }

        return response;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getById(@PathVariable("id") String id, @RequestHeader("Authorization") String authorization, HttpServletResponse http) {
        Object response= new Object();
        try {
            SolicitarrcApi apiInstance = new SolicitarrcApi();
            response = apiInstance.solicitarrcGet0(id, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object deleteById(
            @PathVariable("id") String id,
            @RequestParam String statecode,
            @RequestParam String statuscode,
            @RequestHeader("Authorization") String authorization,
            HttpServletResponse http) {
        Object response= new Object();
        try {
            SolicitarrcApi apiInstance = new SolicitarrcApi();
            apiInstance.solicitarrcDelete(id, statecode, statuscode, authorization);

            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @PutMapping(value = "/{id}",  produces = MediaType.APPLICATION_JSON_VALUE)
    public Object updateById(
            @PathVariable("id") String id,
            @RequestHeader("Authorization") String authorization,
            @RequestBody final CSolicitarRCAttributes value, HttpServletResponse http) {
        Object response= new Object();
        try {
            SolicitarrcApi apiInstance = new SolicitarrcApi();
            response = apiInstance.solicitarrcPut(id, value, authorization);
            
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Object save(
            @RequestHeader("Authorization") String authorization,
            @RequestBody final CSolicitarRCAttributes value, HttpServletResponse http) {
        Object response= new Object();
        try {
            SolicitarrcApi apiInstance = new SolicitarrcApi();
            response = apiInstance.solicitarrcPost(value, authorization);
           
            
        } catch (ApiException e) {
            			response=RESULT_ERROR_START + "" + e.getResponseBody() + RESULT_ERROR_END;
        }
        return response;
    }

}
