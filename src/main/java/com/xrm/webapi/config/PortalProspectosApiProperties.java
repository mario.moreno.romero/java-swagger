package com.xrm.webapi.config;
import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "server")
public class PortalProspectosApiProperties {
    private int port;
    private String apiCrm;
	
	public void setPort(int port) {
		this.port=port;
	}
	public void getApiCrm(String apiCrm) {
		this.apiCrm=apiCrm;
	}
	
	public int getPort() {
		return this.port;
	}
	public String getApiCrm() {
		return this.apiCrm;
	}
   
}